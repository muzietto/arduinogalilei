# ArduinoGalilei
Execution of Galilei experiments on the fall of rigid bodies.

 * five LDR sensors to measure positions as well as velocity (the latter in two distinct points)
 * output through serial monitor
 * calibration of LDR sensors before each test campaign
 * measurement of passage times over an incline
 * suspension and release of metal ball through electromagnetic coil
 * transistor-based relais circuit to govern the coil

Complete final product is _04_arduino_galilei_experiment